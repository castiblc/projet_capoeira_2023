def kdp_leastsquare_single_window(
        radar, wind_len=11, min_valid=6, phidp_field=None, kdp_field=None,
        vectorize=False):
    """
    Compute the specific differential phase (KDP) from differential phase data
    using a piecewise least square method. For optimal results PhiDP should
    be already smoothed and clutter filtered out.

    Parameters
    ----------
    radar : Radar
        Radar object.
    wind_len : int
        The lenght of the moving window.
    min_valid : int
        Minimum number of valid bins to consider the retrieval valid
    phidp_field : str
        Field name within the radar object which represent the differential
        phase shift. A value of None will use the default field name as
        defined in the Py-ART configuration file.
    kdp_field : str
        Field name within the radar object which represent the specific
        differential phase shift. A value of None will use the default field
        name as defined in the Py-ART configuration file.
    vectorize : bool
        whether to use a vectorized version of the least square method

    Returns
    -------
    kdp_dict : dict
        Retrieved specific differential phase data and metadata.

    """
    # parse the field parameters
    if phidp_field is None:
        phidp_field = get_field_name('differential_phase')
    if kdp_field is None:
        kdp_field = get_field_name('specific_differential_phase')

    # extract fields from radar
    radar.check_field_exists(phidp_field)
    phidp = radar.fields[phidp_field]['data']

    kdp_dict = get_metadata(kdp_field)
    if vectorize:
        kdp_dict['data'] = leastsquare_method_scan(
            phidp, radar.range['data'], wind_len=wind_len,
            min_valid=min_valid)
    else:
        kdp_dict['data'] = leastsquare_method(
            phidp, radar.range['data'], wind_len=wind_len,
            min_valid=min_valid)

    return kdp_dict

def leastsquare_method(phidp, rng_m, wind_len=11, min_valid=6):
    """
    Compute the specific differential phase (KDP) from differential phase data
    using a piecewise least square method. For optimal results PhiDP should
    be already smoothed and clutter filtered out.

    Parameters
    ----------
    phidp : masked array
        phidp field
    rng_m : array
        radar range in meters
    wind_len : int
        the window length
    min_valid : int
        Minimum number of valid bins to consider the retrieval valid

    Returns
    -------
    kdp : masked array
        Retrieved specific differential phase field

    """
    # we want an odd window
    if wind_len % 2 == 0:
        wind_len += 1
    half_wind = int((wind_len-1)/2)

    # initialize kdp
    nrays, nbins = np.shape(phidp)
    kdp = np.ma.zeros((nrays, nbins))
    kdp[:] = np.ma.masked
    kdp.set_fill_value(get_fillvalue())

    rng_wind = rolling_window(rng_m/1000., wind_len)
    for ray in range(nrays):
        phidp_ray = phidp[ray, :]
        valid = np.logical_not(np.ma.getmaskarray(phidp_ray))
        valid_wind = rolling_window(valid, wind_len)
        mask_wind = np.logical_not(valid_wind)
        nvalid = np.sum(valid_wind, axis=-1, dtype=int)
        ind_valid = np.logical_and(
            nvalid >= min_valid, valid[half_wind:-half_wind]).nonzero()
        nvalid = nvalid[ind_valid]
        rng_wind_ma = np.ma.masked_where(mask_wind, rng_wind)
        phidp_wind = rolling_window(phidp_ray, wind_len)

        rng_sum = np.ma.sum(rng_wind_ma, -1)[ind_valid]
        rng_sum2 = np.ma.sum(np.ma.power(rng_wind_ma, 2.), -1)[ind_valid]

        phidp_sum = np.ma.sum(phidp_wind, -1)[ind_valid]
        rphidp_sum = np.ma.sum(phidp_wind * rng_wind_ma, -1)[ind_valid]

        kdp[ray, ind_valid[0]+half_wind] = (
            0.5*(rphidp_sum-rng_sum*phidp_sum/nvalid) /
            (rng_sum2-rng_sum*rng_sum/nvalid))

    return kdp
