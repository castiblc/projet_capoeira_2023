import datetime
import getpass
import platform
import warnings

import netCDF4
import numpy as np

import pyart
from pyart.config import FileMetadata

#from ..config import FileMetadata
#from .common import stringarray_to_chararray, _test_arguments
#from ..core.radar import Radar
#from ..lazydict import LazyLoadDict

from pyart.core.radar import Radar

def read_xport(filename, field_names=None, additional_metadata=None,
                  file_field_names=False, exclude_fields=None,memory=None,
                  include_fields=None, delay_field_loading=False, **kwargs):


    # open the file
    dataset = netCDF4.Dataset(filename,memory=memory)
    ncattrs = dataset.ncattrs()
    ncvars = dataset.variables
    

    lat = getattr(dataset, 'RADAR_LAT')
    latitude = dict()
    latitude['data'] = np.array([lat], dtype='float64')
    longitude = dict()
    lon = getattr(dataset, 'RADAR_LON')
    longitude['data'] = np.array([lon], dtype='float64')
    
    altitude = dict()
    altitude['data'] = np.array([0], dtype='float64')
    
    scan_type = getattr(dataset, 'SCAN').lower()

    var = ncvars['Time']
    time = dict((k, getattr(var, k)) for k in var.ncattrs())
    time['data'] = var[:]
    time['units']='seconds since 1970-1-1 0:00:00 0:00'
    
    var = ncvars['Range']
    _range = dict((k, getattr(var, k)) for k in var.ncattrs())
    _range['data'] = var[:]
    

    var = ncvars['Azimut']
    azimuth = dict((k, getattr(var, k)) for k in var.ncattrs())
    azimuth['data'] = var[:]
    
    var = ncvars['Elevation']
    elevation = dict((k, getattr(var, k)) for k in var.ncattrs())
    elevation['data'] = var[:]

    fields = dict()

    var = ncvars['Zh']
    zh = dict((k, getattr(var, k)) for k in var.ncattrs())
    zh['data'] = var[:]
    fields['reflectivity_horizontal'] = zh
    
    var = ncvars['Zv']
    zv = dict((k, getattr(var, k)) for k in var.ncattrs())
    zv['data'] = var[:]
    fields['reflectivity_vertical'] = zv
    
    var = ncvars['Phidp']
    phidp = dict((k, getattr(var, k)) for k in var.ncattrs())
    phidp['data'] = var[:] + 5
    fields['differential_phase'] = phidp
    
    var = ncvars['RhoHV']
    rhohv = dict((k, getattr(var, k)) for k in var.ncattrs())
    rhohv['data'] = var[:] / 100.
    fields['cross_correlation_ratio'] = rhohv

    var = ncvars['VmeanH']
    velocity = dict((k, getattr(var, k)) for k in var.ncattrs())
    velocity['data'] = var[:]
    fields['velocity'] = velocity
    
    
    
    a = np.ndarray(zh['data'].shape)
    a.fill(1)
    normalized_coherent_power = dict()
    normalized_coherent_power['data'] = a
    fields['normalized_coherent_power'] = normalized_coherent_power
        
    
    ngates = len(zh['data'][0])
    nray = len(zh['data'][:,0])
    
    filemetadata = FileMetadata('xport', field_names, additional_metadata,
                                file_field_names, exclude_fields,
                                include_fields)
    
    temp = filemetadata('temperature')
    temp['data'] = np.ones((nray,ngates)) 
    fields['temperature'] = temp



    fixed_angle = filemetadata('fixed_angle')
    fixed_angle['data'] = [elevation['data'][0]]
    
    nsweeps=1
    sweep_number = dict()
    sweep_number['data'] = np.array([0], dtype='int32')
    sweep_start_ray_index = filemetadata('sweep_start_ray_index')
    sweep_start_ray_index['data'] = np.array([0], dtype='int32')
    sweep_end_ray_index = filemetadata('sweep_end_ray_index')
    sweep_end_ray_index['data'] = np.array([nray-1], dtype='int32')
    

    metadata = filemetadata('metadata')
    sweep_mode = filemetadata('sweep_mode')
    sweep_mode['data'] = np.array(nsweeps * ['ppi'])
    

    
    instrument_parameters = dict()
    # instrument_parameters
    prt = filemetadata('prt')
    prt_mode = filemetadata('prt_mode')
    nyquist_velocity = filemetadata('nyquist_velocity')
    unambiguous_range = filemetadata('unambiguous_range')
    beam_width_h = filemetadata('radar_beam_width_h')
    beam_width_v = filemetadata('radar_beam_width_v')
    frequency = filemetadata('frequency')
    frequency['data'] = np.array([9400000000], dtype='float64')
#    instrument_parameters['frequency'] = frequency
    
    
    prt['data'] = dataset.PRF
    if dataset.PRF_TYPE == 'SIMPLE' : 
        prt_mode['data'] =   np.array(['S'])
    else : 
        prt_mode['data'] =  np.array(['T'])
        
    nyquist_velocity['data'] = (299792458 / frequency['data']) * 1000 / 4
    unambiguous_range['data'] = (299792458 * (1/prt['data'] - 10**-6))/2
    beam_width_h['data'] = np.array([1.43],
                                    dtype='float32')
    beam_width_v['data'] = np.array([1.41],
                                    dtype='float32')
    
    antenna_transition=dict()
    antenna_transition["data"]=np.array([0], dtype='int32')
    
    instrument_parameters = {'frequency':frequency,
                             'unambiguous_range': unambiguous_range,
                             'prt_mode': prt_mode, 'prt': prt,
                             'nyquist_velocity': nyquist_velocity,
                             'radar_beam_width_h': beam_width_h,
                             'radar_beam_width_v': beam_width_v}
    
    metadata = {
    'Conventions': 'CF/Radial instrument_parameters',
    'version': '1.0',
    'title': 'Xport radar data file to Py-ART',
    'institution': ('IGE / IRD'),
    'references': 'none',
    'source': 'Xport Radar',
    'history': 'created by D. Regneau and F. Cazenave 2022',
    'comment': 'none',
    'instrument_name': 'Xport'}

    return Radar(
        time, _range, fields, metadata, scan_type,
        latitude, longitude, altitude,
        sweep_number, sweep_mode, fixed_angle, sweep_start_ray_index,
        sweep_end_ray_index,
        azimuth, elevation,
#        antenna_transition=antenna_transition,
        instrument_parameters=instrument_parameters)

