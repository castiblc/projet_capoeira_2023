def arrow(fig,ax1,ax2,x1,y1,x2,y2,color='k'):
    # Create the arrow
    # 1. Get transformation operators for axis and figure
    ax0tr = ax1.transData # Axis 0 -> Display
    ax1tr = ax2.transData # Axis 1 -> Display
    fig.canvas.draw()
    figtr = fig.transFigure.inverted() # Display -> Figure
    # 2. Transform arrow start point from axis 0 to figure coordinates
    ptB = figtr.transform(ax0tr.transform((x1, y1)))
    # 3. Transform arrow end point from axis 1 to figure coordinates
    ptE = figtr.transform(ax1tr.transform((x2, y2)))
    # 4. Create the patch
    arrow = patches.FancyArrowPatch(
        ptB, ptE, transform=fig.transFigure,  # Place arrow in figure coord system
        fc = color, arrowstyle='simple', alpha = 0.3,
        mutation_scale = 40.
    )
    fig.patches.append(arrow)

def add_cumsum(radar,var) :
    raw = copy.deepcopy(radar.fields[var])
    raw['data'] = raw['data'].cumsum(axis=1)
    radar.add_field(var+' cum', raw,replace_existing=True)
    print("Field \""+var+' cum'+"\" added")
    
def clean_phidp_raw(radar,gate0_dphidp=0,unfolded=False):
    mask =[]
    [mask.append(True) for i in range(gate0_dphidp)]
    [mask.append(False) for i in range(len(radar.range["data"])-gate0_dphidp)]
    mask=np.array([mask] * len(radar.azimuth["data"]))

    gatefilter = pyart.correct.GateFilter(radar)
    pyart.correct.GateFilter.exclude_gates(gatefilter,mask)

    phi_dp = copy.deepcopy(radar.fields['differential_phase'])
    if unfolded :
        phi_dp['data'] = 180 - phi_dp['data']
    for i in range(len(radar.azimuth["data"])) : 
        phi_dp['data'][i,...] -= phi_dp['data'][i,gate0_dphidp]
        phi_dp['data'][i,0:gate0_dphidp] = 0
    radar.add_field('differential_phase_init', phi_dp,replace_existing=True)
    return radar, gatefilter
