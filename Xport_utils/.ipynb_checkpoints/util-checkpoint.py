{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a3d50fbf",
   "metadata": {},
   "outputs": [],
   "source": [
    "def arrow(fig,ax1,ax2,x1,y1,x2,y2):\n",
    "    # Create the arrow\n",
    "    # 1. Get transformation operators for axis and figure\n",
    "    ax0tr = ax1.transData # Axis 0 -> Display\n",
    "    ax1tr = ax2.transData # Axis 1 -> Display\n",
    "    fig.canvas.draw()\n",
    "    figtr = fig.transFigure.inverted() # Display -> Figure\n",
    "    # 2. Transform arrow start point from axis 0 to figure coordinates\n",
    "    ptB = figtr.transform(ax0tr.transform((x1, y1)))\n",
    "    # 3. Transform arrow end point from axis 1 to figure coordinates\n",
    "    ptE = figtr.transform(ax1tr.transform((x2, y2)))\n",
    "    # 4. Create the patch\n",
    "    arrow = patches.FancyArrowPatch(\n",
    "        ptB, ptE, transform=fig.transFigure,  # Place arrow in figure coord system\n",
    "        fc = \"g\", arrowstyle='simple', alpha = 0.3,\n",
    "        mutation_scale = 40.\n",
    "    )\n",
    "    fig.patches.append(arrow)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
