#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 15:53:02 2022

@author: fred
"""
import pandas as pd
import numpy as np
import os.path
import time
from datetime import datetime, timedelta
import pytz
from urllib.request import urlopen

EPOCH_UNITS = "seconds since 1970-1-1 00:00:00+0:00"

mto_filename='/Users/fred/Data/Meteo/Campus_fr_OsugB_Mto_01_Min.dat'
mto_filename='/Users/fred/Data/Meteo/CARE_Meteo/CARE_vn_HCMC_Mto_Data_10_Min.dat'

# try :
#     exec(open(dirname(__file__)+'/../../../Utils/ftp_update_file.py').read())
# except:
#     exec(open('../../../Utils/ftp_update_file.py').read())
    
    
# file_update = os.path.getmtime(r'/Users/fred/Data/Meteo_OSUGB/Campus_fr_OsugB_Mto_01_Min.dat')
# now = time.time()
# if (now-file_update) > 300 :
#         ftp_update_file('loggernet.ige-grenoble.fr',mto_filename,ftp_dir_file='/Campus/OSUGB',login='loggernet-ftp')


def extrat_events(mto,sep_event=3600,min_acc=None):
    index=np.where(mto.Rain_mesure_Tot["data"] > 0)[0]
    Rtime=mto.time["data"][index]
    Rrain=mto.Rain_mesure_Tot["data"][index]
    istarts = np.insert(np.where(np.diff(Rtime) > sep_event)[0]+1,0,0)
    istops = np.append(np.where(np.diff(Rtime) > sep_event)[0],len(Rtime)-1)
    acc=list()
    starts=list()
    stops=list()
    ind=0
    
    if min_acc != None :
        for i in range(len(istarts)):
           if Rrain[istarts[i]:istops[i]].sum() > min_acc :
                print("Event ",
                      ind,
                      ": ",
                      pd.to_datetime(Rtime[istarts[i]], unit='s'),
                      " to ",
                      pd.to_datetime(Rtime[istops[i]], unit='s'),
                      " : rain acc = ",
                      round(Rrain[istarts[i]:istops[i]].sum(),2))
                acc.append(round(Rrain[istarts[i]:istops[i]].sum(),2))
                starts.append(np.where(mto.time["data"] == Rtime[istarts[i]])[0])
                stops.append(np.where(mto.time["data"] == Rtime[istops[i]])[0])
                ind += 1
    return starts,stops,acc


def read_meteo_file(mto_filename,start=None,stop=None) :
    """
    read a Meteo data file format OSUG and CARE and returns a MeteoReaderCampbell object.
    start : define the starting date of sampling extraction - the nearest will be used
    if start is not set, extraction will start at the begining of the sampling 
    stop  : define the stoping date of the sampling extraction - the nearest will be used
    if stop is not set, extraction will end at the ending of the sampling 
    if start and stop are not set, all the sampling will be passed to the dsd object
    start and stop should be '%Y-%m-%d %H:%M:%S'
        
    
    Usage:
    mto = read_meteo_file(mto_filename,start=None,stop=None)
    
    Returns:
    MTO_OSUGB object
    
    """   
    
    if (mto_filename.find('http')!=0 and os.path.exists(mto_filename)!=True and mto_filename.find('ftp')!=0) :
        print ("Spect_filename doesn't exist !")
        return None
 
    if start != None :
        try :
            start = pd.to_datetime(start, utc= True)
        except :
                print ("Start date format is not correct, try with %Y-%m-%d %H:%M:%S")
                return None
    
    if stop != None :
        try :
            stop = pd.to_datetime(stop, utc= True)
        except :
            print ("Stop date format is not correct, try with %Y-%m-%d %H:%M:%S")
            return None
    
    meteo = MeteoReaderCampbell(mto_filename,start=start,stop=stop)
  
    return meteo  


class MeteoReaderCampbell(object):

    """
    MeteoReaderCampbell class takes one filename as argument.
    start an stop period could be specified
    resampling is given in seconds
    
    """

    def __init__(self, mto_filename,start=None,stop=None):
        self.mto_filename = mto_filename
        self.name = ""
        self.start = start
        self.stop = stop 
        self.AirTemp_Avg=[]
        self.RH_Avg = []  
        self.Trosee_Avg = []
        self.WSpd_Avg = []
        self.WDir_Avg = []
        self.WDir_Std = []
        self.WSpd_Max = []
        self.Rain_mesure_Tot = []
        self.Ray_Global_Avg = []
        self.time = []
        
        self._ReaderMeteo()
        
    def _ReaderMeteo(self):

        if (self.mto_filename.find('http')!=0 and os.path.exists(self.mto_filename)!=True and self.mto_filename.find('ftp')!=0) :
            print ("filename ", self.mto_filename," doesn't exist !")
            return None
        
        if os.path.exists(self.mto_filename): 
            fd = open(self.mto_filename)
            headers = [ next(fd) for i in range(4) ]
            meta=headers[0].replace('"' , '').replace('\n','').replace('\r','').split(',')
            cols = headers[1].replace('"' , '').replace('\n','').split(',')
            units= headers[2].replace('"' , '').replace('\n','').split(',')
            df = pd.read_csv(fd, sep=',', decimal='.', names=cols,index_col=0,
                             parse_dates=True,na_values=["NAN", "INF", "-INF"], low_memory=False)
            fd.close()
        else :
            fd = urlopen(self.mto_filename)
            headers = [ next(fd).decode("UTF-8") for i in range(4) ]
            meta=headers[0].replace('"' , '').replace('\n','').replace('\r','').split(',')
            cols = headers[1].replace('"' , '').replace('\n','').split(',')
            units= headers[2].replace('"' , '').replace('\n','').split(',')
            df = pd.read_csv(fd, sep=',', decimal='.', names=cols,index_col=0,encoding='utf8',
                             parse_dates=True,na_values=["NAN", "INF", "-INF"], low_memory=False)
            fd.close()
        
        self.name = meta[1]
        # Modif to take in acount TZ at CARE FC - 22/11/22
        if "vn" in self.name : 
            print ("Time Zone="+ str(pytz.timezone('Asia/Ho_Chi_Minh')))
            HCMC_now=datetime.now(pytz.timezone('Asia/Ho_Chi_Minh'))
            offset=HCMC_now.utcoffset().total_seconds()
            print(offset)
        else :
            offset=0
            
        sampling = meta[-1][1:]
        
        

        if self.start != None :
           print(self.start)
           index=df.index[np.where(pd.to_datetime(df.index, utc = True) >= self.start + timedelta(seconds=offset))]
           
        if self.stop != None :
           print(self.stop)
           index=index[np.where(pd.to_datetime(index, utc = True) <= self.stop + timedelta(seconds=offset))]
        
        time_epoch = (index - datetime.fromtimestamp(0)) / np.timedelta64(1, 's')
          
        self.time = {
            "data": time_epoch.values - offset,
            "units": EPOCH_UNITS,
            "title": "Time",
            "long_name": "time",
        }
        if "AirTemp_Avg" in cols : 
            ind = cols.index("AirTemp_Avg") 
        else :
            ind = cols.index("Air_Temp_Avg")
        self.AirTemp_Avg = {
            "data": df[cols[ind]][index],
            "units": units[ind],    
            "title" : "AirTemp_Avg",
            "long_name": "Air Temperature Average",
        }
        if "RH_Avg" in cols : 
            ind = cols.index("RH_Avg") 
        else :
            ind = cols.index("Air_RH_Avg")
        self.RH_Avg = {
            "data": df[cols[ind]][index],
            "units": units[ind],
            "title" : "RH_Avg",
            "long_name": "Relative Humidity",
        } 
        if "Trosee_Avg" in cols : 
            self.Trosee_Avg = {
                "data": df.Trosee_Avg[index],
                "units": units[cols.index("Trosee_Avg")],
                "title" : "Trosee_Avg",
                "long_name": "Dewpoint Temperature Average",
            } 
        self.WSpd_Avg = {
            "data": df.WSpd_Avg[index],
            "units": units[cols.index("WSpd_Avg")],
            "title" : "RH_Avg",
            "long_name": "Relative Humidity",
        } 
        self.WDir_Avg = {
            "data": df.WDir_Avg[index],
            "units": units[cols.index("WDir_Avg")],
            "title" : "WSpd_Avg",
            "long_name": "Wind Speed Average",
        } 
        self.WDir_Std = {
            "data": df.WDir_Std[index],
            "units": units[cols.index("WDir_Std")],
            "title" : "WDir_Std",
            "long_name": "Wind Direction Std",
        } 
        self.WSpd_Max = {
            "data": df.WSpd_Max[index],
            "units": units[cols.index("WSpd_Max")],
            "title" : "WSpd_Max",
            "long_name": "Wind Max Speed",
        } 
        if "Rain_mesure_Tot" in cols : 
            ind = cols.index("Rain_mesure_Tot") 
        else :
            ind = cols.index("Rain_Tot")
        self.Rain_mesure_Tot = {
            "data": df[cols[ind]][index],
            "units": units[ind],
            "title" : "Rain_mesure_Tot",
            "long_name": "Rain accumulation during sampling interval ",
        }
        
    
        
        
