#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 17 10:00:05 2021

@author: fred
"""
import matplotlib as mpl
import datetime as dt
import pandas as pd
import numpy as np
import copy
from pylab import figure, ylim, show, savefig, plot_date, date2num,twinx,contour,legend,colorbar
import math
from matplotlib.dates import YearLocator, MonthLocator,DayLocator,HourLocator,MinuteLocator,DateFormatter
from matplotlib.ticker import AutoLocator, MultipleLocator, FixedLocator

def air_density(temp=15,pressure=1013.25):
    """ Calculate  the density of the air

        Parameters
        ----------
        temp: float, optional
            Air temperature at sensor level in °C.  Defaults to 15°C.
        pressure: float, optional
            Atmospheric prssure at sensor level in hPa. Defaults to 1013.25hPa. 
        
        Returns
        -------
        air_density : float
            air densité at the vincinity of the sensor in g/m3

        """
    return (pressure * 100 / ((273.15 + temp)*287.058))*1000

'''
extract Start and stop event date -  sep_event is the time with no rain separating 2 events 
#Data split in rain event could be done over
   - 2 events are plit by a dry period of time (sep_event by default is set to 3h) 
   - a minimum number drops (min_drops) could be set to considere a rainy period as a event
   - a minimum of rain acculalation (min_acc) could be set to considere a rainy period as a event
   - if min_drops is set min_acc is not used
''' 
def get_events(dsd,sep_event=10800,min_drops=None,min_acc=None):
    starts = np.insert(np.where(np.diff(dsd.time["data"]) > sep_event)[0]+1,0,0)
    stops = np.append(np.where(np.diff(dsd.time["data"]) > sep_event)[0],len(dsd.time["data"])-1)
    event=list()
    ind = 0
    if min_drops != None : 
        for i in range(len(starts)):
            if dsd.fields["drop_spectrum"]["data"][starts[i]:stops[i]+1,:,:].sum() >= min_drops :
                print("Event ",
                      ind,
                      ": ",
                      pd.to_datetime(dsd.time["data"][starts[i]], unit='s'),
                      " to ",
                      pd.to_datetime(dsd.time["data"][stops[i]], unit='s'),
                      " : rain acc = ",
                      round(get_rain_accumulation (dsd,starts[i],stops[i]+1),2)
                      if isinstance(get_rain_accumulation (dsd,starts[i],stops[i]+1),float)
                      else "NaN",
                      " ,Nd = ",
                      np.nansum(dsd.fields["drop_spectrum"]["data"][starts[i]:stops[i]+1,:,:]),
                      " (",starts[i],",",stops[i]+1,")")
                event.append((dsd.time["data"][starts[i]],dsd.time["data"][stops[i]]))
                ind += 1
        return event
   
    if min_acc != None :
        for i in range(len(starts)):
           if get_rain_accumulation (dsd,starts[i],stops[i]) > min_acc :
                print("Event ",
                      ind,
                      ": ",
                      pd.to_datetime(dsd.time["data"][starts[i]], unit='s'),
                      " to ",
                      pd.to_datetime(dsd.time["data"][stops[i]], unit='s'),
                      " : rain acc = ",
                      round(get_rain_accumulation (dsd,starts[i],stops[i]+1),2),
                      " ,Nd = ",
                      np.nansum(dsd.fields["drop_spectrum"]["data"][starts[i]:stops[i]+1,:,:]))
                event.append((dsd.time["data"][starts[i]],dsd.time["data"][stops[i]]))
                ind += 1
        return event
    
    for i in range(len(starts)):
        print("Event ",
              ind,
              ": ",
              pd.to_datetime(dsd.time["data"][starts[i]], unit='s'),
              " to ",
              pd.to_datetime(dsd.time["data"][stops[i]], unit='s'),
              " : rain acc = ",
              round(get_rain_accumulation (dsd,starts[i],stops[i]+1),2),
              " ,Nd = ",
              np.nansum(dsd.fields["drop_spectrum"]["data"][starts[i]:stops[i]+1,:,:]))
        event.append((dsd.time["data"][starts[i]],dsd.time["data"][stops[i]]))
        ind += 1
    return event        
      
def get_rain_accumulation (dsd,start=0,stop=0): 
    # compute rain accumalation nbetween to dates
    if start > stop :
        print("dates error")
        return 0
    else :
        return np.nansum(dsd.fields["rain_rate"]["data"][start:stop])*np.nanmin(dsd.sampling_interval["data"])/3600
                                
def extract_subset_dsd (dsd,index):
   n_dsd = copy.deepcopy(dsd)
   n_dsd.time["data"]=np.array(dsd.time["data"][index])
   try:
       n_dsd.rain_rate["data"] = dsd.rain_rate["data"][index]
   except:
       n_dsd.rain_rate = None
   try:
       n_dsd.Z["data"] = dsd.Z["data"][index]
   except:
       n_dsd.Z = None
   try:
       n_dsd.sampling_interval["data"]  = np.copy(dsd.sampling_interval["data"][index])
   except:
       n_dsd.sampling_interval  = None

   n_dsd.numt = len(dsd.time["data"][index])
       
   listvar=[*dsd.fields.keys()]
   listvar.remove("spectrum_fall_velocity")

   for varname in listvar: 
       if varname == 'drop_spectrum' : 
           n_dsd.fields[varname]["data"] = np.copy(dsd.fields[varname]["data"][index])
       else :
           n_dsd.fields[varname]["data"] = dsd.fields[varname]["data"][index].copy()
   
   if np.any(np.diff(n_dsd.time["data"]) != np.nanmin(np.diff(n_dsd.time["data"]))):
        n_dsd=make_sampling_interval(n_dsd)

   return n_dsd
        
   
                     # a transcrire
# def closest_event(dsd,date=None):
#     return dsd.df.index.to_series()[(np.abs(dsd.df.index.to_series()-date)).argmin()]

# def closest_start_event(list_start_event=None,date=None):
#     return list_start_event[list_start_event.to_series().le(date)][-1]

# def closest_stop_event(list_stop_event=None,date=None):
#     return list_stop_event[list_stop_event.to_series().gt(date)][0]

def get_xtick_interval(xmin,xmax):
    nticks  = 8
    deltat  = (xmax-xmin)*24.
    #delta_h = int(math.ceil(deltat/nticks)) 
    delta_h = deltat/nticks
    if delta_h > 0.5:
        return ('hour',int(math.ceil(delta_h)),)
    if delta_h <= 0.5:
        return ('minute',int(math.ceil(deltat*60)/nticks),)
    #print 'delta_h in get_xtick_interval: ', delta_h
    #return delta_h
    
def get_data(data, varname):
    requested_var = pd.Series(data=None,dtype='float64')
    for i in varname:
        if requested_var.size != 0:
            requested_var=pd.concat([requested_var, data[i]], axis=1)
        else:
            requested_var=data[i]
    return requested_var

# Unsufull function as data are not row 
# def pars2_to_dsd(data,bins):
#     data[data.lt(0)]=float('NaN')
#     for i in range(data.shape[1]-1):
#         data[data.columns[i]]=data[data.columns[i]]/(0.0054*60*9.65-10.3**(0.6*data[data.columns[i]])*bins[i])
#     return data
    

def make_regular_TS(dsd):
   #  add missing Time Step
  freq=int(dsd.sampling_interval["data"].min())
  # treatment for dsd.fields variables
  df = pd.DataFrame({'dt': pd.to_datetime(dsd.time["data"], unit='s'), "rain_rate" : np.array(dsd.fields["rain_rate"]["data"])})
  listvar=[*dsd.fields.keys()]
  listvar.remove("rain_rate")
  listvar.remove("spectrum_fall_velocity")
  for varname in listvar: 
          print(varname)
          if dsd.fields[varname] != None : 
              if varname =='Nd': 
                  for i in range((dsd.fields[varname]["data"].shape)[1]) :
                      df.insert(loc=len(df.columns),column=varname+str(i), value=dsd.fields[varname]["data"][:,i])
              elif varname == 'drop_spectrum' :
                  for i in range((dsd.fields[varname]["data"].shape)[1]) :
                      for j in range((dsd.fields[varname]["data"].shape)[2]) : 
                        df.insert(loc=len(df.columns),column=varname+str(i*(dsd.fields[varname]["data"].shape)[1]+j), value=dsd.fields[varname]["data"][:,i,j]) 
                        df = df.copy()
              else :
                      df.insert(loc=len(df.columns),column=varname, value=dsd.fields[varname]["data"])
          else :
              listvar.remove(varname)
  df = df.copy()
  r = pd.date_range(start=df.dt.min(), end=df.dt.max(),freq=f"{freq}s")
  #df = df.set_index('dt').reindex(r).fillna(0.0).rename_axis('dt').reset_index()
  df = df.set_index('dt').reindex(r).rename_axis('dt').reset_index()
  df = df.copy()
  df.rain_rate=df.rain_rate.fillna(0.0)
  listvar.append("rain_rate")
  listvar.append("time")  
  dsd2 = copy.deepcopy(dsd)
  for varname in listvar: 
          print(varname)
          if varname == 'drop_spectrum' : 
              cols_DS = [i for i in df.columns if "drop_spectrum" in i]          
              dsd2.fields["drop_spectrum"]["data"]= np.reshape(df[cols_DS].loc[df.index].values,(len(df.dt),32,32)) #Nd
          elif varname == 'Nd' :
              cols_Nd= [i for i in df.columns if "Nd" in i]
              mask=np.reshape(df[cols_Nd].isna(),(len(df.dt),32))
              dsd2.Nd["data"] = np.ma.array(np.reshape(df[cols_Nd].loc[df.index].values,(len(df.dt),32)),mask=mask)
              dsd2.fields["Nd"]=dsd2.Nd.copy()
          elif varname == 'time' : 
              dsd2.time["data"] = np.array([ dt.datetime.timestamp(df.dt[i]) for i in range(len(df.dt))])
          else :
              mask=df[varname].isna()          
              dsd2.fields[varname]["data"] = np.ma.array(df[varname],mask=mask)
#          dsd2.fields[varname]["data"]=dsd2.fields[varname]["data"].fillna(0.0)
              
  df = pd.DataFrame({'dt': pd.to_datetime(dsd.time["data"], unit='s'), "Z" : np.array(dsd.Z["data"])})
  df.insert(loc=len(df.columns),column='num_particles', value=dsd.num_particles["data"])
  df = df.set_index('dt').reindex(r).rename_axis('dt').reset_index()
  mask=df['Z'].isna()   
  dsd2.Z["data"] = np.ma.array(df['Z'],mask=mask)  
  dsd2.num_particles["data"] = np.ma.array(df['num_particles'],mask=mask)
  dsd2.rain_rate=dsd2.fields["rain_rate"].copy()
  dsd2.numt=len(df.index)
  del(df)
  return dsd2

def conv_strat_bringi_09 (dsd) :
    is_above = lambda p,a,b: np.cross(p-a, b-a) < 0
    BR09_a=-1.6
    BR09_b=6.3   
    a= np.array([1,BR09_a+BR09_b])
    b= np.array([3,BR09_a*3+BR09_b])
    y=np.log10(dsd.fields["Nw"]["data"])
    x=dsd.fields["D0"]["data"]    
    return [is_above([x[i],y[i]],a,b) for i in range(dsd.numt)]

def conv_strat_Thompson_15 (dsd) :
    is_above = lambda p,a,b: np.cross(p-a, b-a) < 0
    T15_b=3.75   
    a= np.array([1,T15_b])
    b= np.array([3,T15_b])
    y=np.log10(dsd.fields["Nw"]["data"])
    x=dsd.fields["D0"]["data"]    
    return [is_above([x[i],y[i]],a,b) for i in range(dsd.numt)]

def get_GammaDSD (dsd,D_max=None) :
    D0 = np.nanmean(dsd.fields["D0"]["data"])
    mu = np.nanmean(dsd.fields["mu"]["data"])
    D_max = 3.0*D0 if D_max is None else D_max
    Nw = np.nanmean(dsd.fields["Nw"]["data"])
    nf = Nw * 6.0/3.67**4 * (3.67+mu)**(mu+4)/math.gamma(mu+4)
    d = (dsd.diameter["data"]/D0)
    psd = nf * np.exp(mu*np.log(d)-(3.67+mu)*d)
    return psd

def get_UnnormalizedGammaDSD (dsd,D_max=None) :
    N0=np.nanmean(dsd.fields["N0"]["data"])
    mu = np.nanmean(dsd.fields["mu"]["data"])
    d = dsd.diameter["data"] if D_max is None else dsd.diameter["data"][where(dsd.diameter["data"] <= D_max)]   
    Lambda=np.nanmean(dsd.fields["Lambda"]["data"])
    psd = N0 * np.exp(mu*np.log(d)-Lambda*d)
    return psd

def get_box_plot_data(labels, bp):
    rows_list = []
    for i in range(len(labels)):
        dict1 = {}
        dict1['label'] = labels[i]
        dict1['lower_whisker'] = bp['whiskers'][i*2].get_ydata()[1]
        dict1['lower_quartile'] = bp['boxes'][i].get_ydata()[1]
        dict1['median'] = bp['medians'][i].get_ydata()[1]
        dict1['upper_quartile'] = bp['boxes'][i].get_ydata()[2]
        dict1['upper_whisker'] = bp['whiskers'][(i*2)+1].get_ydata()[1]
        rows_list.append(dict1)
    return pd.DataFrame(rows_list)
    

    
    
    
